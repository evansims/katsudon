Katsudon is a fork of the [TwitterDev Collections Chrome Extension](https://github.com/twitterdev/chrome-extension-collections) which enhances Twitter.com with Collection management features. This fork fixes the broken/obsolete API calls of the legacy extension, continues to expand on it's Collection management features, and adds additional quality of life enhancements.

### Features
Enhances Twitter.com with the following functionality:

- Create, delete and manage Collections and tweets in those collections.
- Curate posts from your Twitter timeline into Collections.
- Generate HTML embeds for individual tweets or whole Collections.

### Planned Features
- Enhanced media viewing
- Improved Collection management interface
- Pagination for Collection lists (currently limited to viewing 20 tweets)

### Installation
Katsudon won't be published to the Chrome store until it's matured.

For now, you'll need to fork the repo and add the repo directory to your
Chrome as an "unpacked extension." This feature is available in your
[Extensions settings](chrome://extensions/) after Developed Mode is enabled.

### Dependencies
The following libraries are used in this extension. All libraries are included
under the js/lib directory to make this source self-contained.

- Bootstrap: http://getbootstrap.com/
- JQuery: http://jquery.com/
- JQuery UI: http://jqueryui.com/
- Mustache.js: https://github.com/janl/mustache.js
- Async: https://github.com/caolan/async
- Codebird (Modified for Twitter Collections): https://github.com/jublonet/codebird-js

### License
Please read the LICENSE file in the root path.
